/**
 * \file vmMemoryAllocator.cpp
 * \brief Implements the Memory Allocator used by the Virtual Machine.
 * \author Glaize, Sylvain
 */

#include "vmMemoryAllocator.hpp"

AnotherWorld::VMMemoryAllocator::VMMemoryAllocator() {
    markedScriptPointer = memory.data();
    currentScriptPointer = memory.data();

    currentVideoPointer = memory.data() + MEM_BLOCK_SIZE - VIDEO_BLOCK_SIZE;
}
