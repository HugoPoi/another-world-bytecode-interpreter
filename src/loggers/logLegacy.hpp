#ifndef __LOGLEGACY_HPP__
#define __LOGLEGACY_HPP__

/**
 * \file logLegacy.hpp
 * \brief class to implement the logs as they were originally designed
 * \author Willll
 */

#include "../ilog.hpp"

namespace AnotherWorld {
    /// \brief Class for legacy logging.
    class LogLegacy : public ILogger {
        public :
            /// \brief Default constructor disabled.
            LogLegacy () = delete;

            /**
             * \brief Constructor.
             * \param isSet Whether or not logging.
             */
            explicit LogLegacy (bool isSet):
                    ILogger(isSet), severityLevel(SeverityLevels::DISABLE) {}

            /// \brief Destructor.
            virtual ~LogLegacy () {}

            /**
             * \brief Sending a log message.
             * \param slvl Message severity level.
             * \param format Message format describer.
             */
            void log (SeverityLevels slvl, const char* format, ...) override;

            /**
             * \brief Sending a debug message.
             * \param cm Code for message to generate.
             * \param format Message format describer.
             */
            void debug (ILogger::DebugSource cm, const char* format,
                        ...) override;

            /**
            * \brief Sending a warning message.
            * \param format Message format describer.
            */
            void information (const char* format, ...) override;

            /**
             * \brief Sending a warning message.
             * \param format Message format describer.
             */
            void warning (const char* format, ...) override;

            /**
             * \brief Sending an error message.
             * \param format Message format describer.
             */
            void error(const char* format, ...) override;

            /**
             * \brief setup logger
             * \param configuration logger configuration object.
             */
            void setup(const LoggerConfiguration &configuration) override;

        protected:
            /// \brief logs severity level.
            SeverityLevels severityLevel;
    };
}

#endif
