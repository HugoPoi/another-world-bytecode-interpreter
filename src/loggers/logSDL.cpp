/**
 * \file logSDL.cpp
 * \brief class to implement the logs through SDL_log
 * \author Willll
 */

#include "logSDL.hpp"

#include <stdexcept>

#include "../errors.hpp"

void AnotherWorld::LogSDL::setup (const LoggerConfiguration &configuration) {
    severityLevel = configuration.getSeverityLevel();
    // Disable console logging if not explicitly asked from CLI interface.
    if (!configuration.isConsoleEnable()) {
        isSet = false;
    }
    else {
        SDL_LogSetPriority(SDL_LOG_CATEGORY_APPLICATION,
                           toSDLLogLevel(severityLevel));
    }
}

void AnotherWorld::LogSDL::log (SeverityLevels slvl, const char* format, ...) {
    if (isSet && slvl >= severityLevel) {
        /// Command line parameters.
        std::va_list va;
        va_start(va, format);
        log(slvl, format, va);
        va_end(va);
    }
}

void AnotherWorld::LogSDL::debug (ILogger::DebugSource cm, const char* format,
                                  ...) {
    if (isSet && cm) {
        /// Command line parameters.
        std::va_list va;
        va_start(va, format);
        log(SeverityLevels::DEBUG, format, va);
        va_end(va);

    }
}

void AnotherWorld::LogSDL::information (const char* format, ...) {
    if (isSet) {
        /// Command line parameters.
        std::va_list va;
        va_start(va, format);
        log(SeverityLevels::INFORMATION, format, va);
        va_end(va);
    }
}

void AnotherWorld::LogSDL::warning (const char* format, ...) {
    if (isSet) {
        /// Command line parameters.
        std::va_list va;
        va_start(va, format);
        log(SeverityLevels::WARNING, format, va);
        va_end(va);
    }
}

void AnotherWorld::LogSDL::error (const char* format, ...) {
    if (isSet) {
        /// Command line parameters.
        std::va_list va;
        va_start(va, format);
        log(SeverityLevels::ERROR, format, va);
        /// Error message.
        char message [1024];
        std::vsprintf(message, format, va);
        va_end(va);
        throw UnrecoverableError (message);
    }
}

SDL_LogPriority AnotherWorld::LogSDL::toSDLLogLevel(const SeverityLevels &severityLevel) const {
    switch (severityLevel) {
        case DEBUG :
            return SDL_LOG_PRIORITY_DEBUG;
        case INFORMATION :
            return SDL_LOG_PRIORITY_INFO;
        case WARNING :
            return SDL_LOG_PRIORITY_WARN;
        case ERROR :
            return SDL_LOG_PRIORITY_ERROR;
        case DISABLE :
        default:
            throw std::runtime_error ("toSDLLogLevel : invalid severityLevel");
            break;
    }
}

void AnotherWorld::LogSDL::log (SeverityLevels slvl, const char *format, std::va_list va) {
    if (isSet && slvl >= severityLevel) {
        /// Command line parameters.
        SDL_LogMessageV(SDL_LOG_CATEGORY_APPLICATION, toSDLLogLevel(slvl), format, va);
    }
}
